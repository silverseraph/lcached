#!/usr/bin/env python3

import sys
import zmq
# import socket
import struct


class Packet:
    EXPIRE = 1  # Announce item as expired
    CACHED = 2  # Announce new cached object
    MQUERY = 3  # Query data at my host
    MCACHE = 4  # Cache data at myself
    LQUERY = 5  # Local cache query from one host to another
    PERROR = 7  # Packet error. Error in data.

    TYPE_DICT = {
        1: 'EXPIRE',
        2: 'CACHED',
        3: 'MQUERY',
        4: 'MCACHE',
        5: 'LQUERY',
        7: 'PERROR'}

    def __init__(self, msg_type, success, del_time, name, sender, data):
        self.msg_type = msg_type  # byte
        if isinstance(success, bool):
            self.success = int(success)
        else:
            self.success = success

        self.del_time = del_time  # int

        if isinstance(name, str):
            self.name = name.encode('utf8')
        else:
            self.name = name          # int = len(name), string = name

        if isinstance(sender, str):
            self.sender = sender.encode('utf8')
        else:
            self.sender = sender

        self.data = bytearray(0) if data is None else data  # bytearray
        if not (isinstance(self.data, bytearray) or
                isinstance(self.data, bytes)):
            if isinstance(self.data, str):
                self.data = self.data.encode('utf8')
            self.data = bytearray(self.data)

    def _type_to_str(msg_type):
        if msg_type in Packet.TYPE_DICT:
            return 'UNKNOWN'
        else:
            return Packet.TYPE_DICT[msg_type]

    def __str__(self):
        fmt = 'Packet{ msg_type = \'%s\', success = %d, del_time = %d, '
        fmt += 'name = \'%s\', sender = \'%s\', data = \'%s\' }'
        return fmt % (
            self.msg_type,
            self.success,
            self.del_time,
            self.name,
            self.sender,
            self.data)

    def binarize(self):
        buff = bytearray(0)
        buff += struct.pack('!B', self.msg_type)
        buff += struct.pack('!B', self.success)
        buff += struct.pack('!i', self.del_time)
        buff += struct.pack('!i', len(self.name))
        buff += struct.pack('!i', len(self.sender))
        buff += struct.pack('!i', len(self.data))
        buff += self.name
        buff += self.sender
        buff += self.data
        return buff

    def debinarize(buff):
        msg_type = buff[0]
        success = buff[1]
        del_time = struct.unpack_from('!i', buff, 2)[0]
        name_len = struct.unpack_from('!i', buff, 6)[0]
        sender_len = struct.unpack_from('!i', buff, 10)[0]
        data_len = struct.unpack_from('!i', buff, 14)[0]

        name = bytearray(buff[18:18 + name_len]).decode('utf8')
        sender = bytearray(
            buff[18 + name_len:18+name_len + sender_len]).decode('utf8')
        data = bytearray(
            buff[18+name_len+sender_len:18+name_len + sender_len + data_len])
        return Packet(msg_type, success, del_time, name, sender, data)


def make_request(sock, tag):
    printf("Making request for: \"%s\"\n", tag)
    pack = Packet(Packet.MQUERY, 0, 0, tag, 'localhost', None)
    try:
        sock.send(pack.binarize())
        rpack = Packet.debinarize(sock.recv())

        if rpack.success:
            printf("Found \"%s\" in the local cache.\n", rpack.name.decode('utf8'))
            print(rpack.data)
        else:
            printf("Failed to retrieve \"%s\" from the local cache.\n", tag)
    except Exception as e:
        print('ERROR: ', e)


def main():
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REQ)
    sock.setsockopt(zmq.RCVTIMEO, 30)
    sock.setsockopt(zmq.SNDTIMEO, 30)
    # sock = ctx.socket('tcp://localhost:%d' % (1335))
    sock.connect('tcp://localhost:%s' % (51335))
    for tag in sys.argv[1:]:
        make_request(sock, tag)

    sock.close()
    ctx.term()


def printf(s, *vargs):
    return sys.stdout.write(s % vargs)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("Caught keyboard interrupt.  Shutting down.")
