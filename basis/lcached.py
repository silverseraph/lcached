#!/usr/bin/env python3

# import os
import sys
import time
import zmq
import threading
import sched
import struct
import socket
# from netifaces import interfaces, ifaddresses, AF_INET  # needed for bcast
# import netifaces


class Packet:
    EXPIRE = 1  # Announce item as expired
    CACHED = 2  # Announce new cached object
    MQUERY = 3  # Query data at my host
    MCACHE = 4  # Cache data at myself
    LQUERY = 5  # Local cache query from one host to another
    PERROR = 7  # Packet error. Error in data.

    TYPE_DICT = {
        1: 'EXPIRE',
        2: 'CACHED',
        3: 'MQUERY',
        4: 'MCACHE',
        5: 'LQUERY',
        7: 'PERROR'}

    def __init__(self, msg_type, success, del_time, name, sender, data):
        self.msg_type = msg_type  # byte
        if isinstance(success, bool):
            self.success = int(success)
        else:
            self.success = success

        self.del_time = del_time  # int

        if isinstance(name, str):
            self.name = name.encode('utf8')
        else:
            self.name = name          # int = len(name), string = name

        if isinstance(sender, str):
            self.sender = sender.encode('utf8')
        else:
            self.sender = sender

        self.data = bytearray(0) if data is None else data  # bytearray
        if not (isinstance(self.data, bytearray) or
                isinstance(self.data, bytes)):
            if isinstance(self.data, str):
                self.data = self.data.encode('utf8')
            self.data = bytearray(self.data)

    def _type_to_str(msg_type):
        if msg_type in Packet.TYPE_DICT:
            return 'UNKNOWN'
        else:
            return Packet.TYPE_DICT[msg_type]

    def __str__(self):
        fmt = 'Packet{ msg_type = \'%s\', success = %d, del_time = %d, '
        fmt += 'name = \'%s\', sender = \'%s\', data = \'%s\' }'
        return fmt % (
            self.msg_type,
            self.success,
            self.del_time,
            self.name,
            self.sender,
            self.data)

    def binarize(self):
        buff = bytearray(0)
        buff += struct.pack('!B', self.msg_type)
        buff += struct.pack('!B', self.success)
        buff += struct.pack('!i', self.del_time)
        buff += struct.pack('!i', len(self.name))
        buff += struct.pack('!i', len(self.sender))
        buff += struct.pack('!i', len(self.data))
        buff += self.name
        buff += self.sender
        buff += self.data
        return buff

    def debinarize(buff):
        if len(buff) < 16:
            raise SyntaxError('Insufficient bytes for header of lcache packet.')

        msg_type = buff[0]
        success = buff[1]
        del_time = struct.unpack_from('!i', buff, 2)[0]
        name_len = struct.unpack_from('!i', buff, 6)[0]
        sender_len = struct.unpack_from('!i', buff, 10)[0]
        data_len = struct.unpack_from('!i', buff, 14)[0]

        if len(buff) - 16 < (name_len + sender_len + data_len):
            raise SyntaxError('Insufficient bytes for data of lcache paacket.')

        name = bytearray(buff[18:18 + name_len]).decode('utf8')
        sender = bytearray(
            buff[18 + name_len:18+name_len + sender_len]).decode('utf8')
        data = bytearray(
            buff[18+name_len+sender_len:18+name_len + sender_len + data_len])
        return Packet(msg_type, success, del_time, name, sender, data)


class CacheItem:
    OWNER_SELF = '127.0.0.1'

    def __init__(self, name, del_time, data=None, owner=OWNER_SELF):
        self.name = name
        self.del_time = del_time
        self.data = data
        self.owner = owner


class DContext:
    BCAST_PORT = 51333
    BCAST_EVENT = 30
    DCOM_PORT = 51334
    ICOM_PORT = 51335
    EXPIRE_EVENT = 1
    # BCAST_ADDR = '[ff02::1]'

    def __init__(self):
        self.mreg = {}  # myself registry
        self.lreg = {}  # local registry
        self.hreg = {}  # host registry
        self.ctx = zmq.Context(5)
        self.scheduler = sched.scheduler(time.time, time.sleep)
        self.scheduler.run()
        self.addr = DContext._find_local_ip()

    def _find_local_ip():
        return socket.gethostbyname(socket.gethostname())

    def _sock_init(self):
        self.dcomr_sock = self.ctx.socket(zmq.REP)
        self.icom_sock = self.ctx.socket(zmq.REP)
        self.bcastr_sock = self.ctx.socket(zmq.SUB)
        self.bcastt_sock = self.ctx.socket(zmq.PUB)

        self.bcastr_sock.setsockopt(zmq.IPV4ONLY, True)
        self.bcastt_sock.setsockopt(zmq.IPV4ONLY, True)

        self.dcomr_sock.bind("tcp://*:%d" % DContext.DCOM_PORT)
        self.icom_sock.bind("tcp://127.0.0.1:%d" % DContext.ICOM_PORT)
        self.bcastt_sock.bind("tcp://%s:%d" % (self.addr, DContext.BCAST_PORT))

    def start(self):
        self.is_running = True
        self._sock_init()

        self.dcom_thread = threading.Thread(target=self._dcom_listen)
        self.icom_thread = threading.Thread(target=self._icom_listen)
        self.cast_thread = threading.Thread(target=self._cast_listen)

        self.dcom_thread.daemon = True
        self.icom_thread.daemon = True
        self.cast_thread.daemon = True

        self.dcom_thread.start()
        self.icom_thread.start()
        self.cast_thread.start()

        self.scheduler.enter(DContext.EXPIRE_EVENT, 0, self._ttl_sweep, ())
        self.scheduler.enter(0,  1, self._autosub,   ())

        self.scheduler.run()
        self.scheduler.run()

    def stop(self):
        self.is_running = False

        self.dcom_thread.join()
        self.icom_thread.join()
        self.cast_thread.join()

    def _ttl_sweep(self):
        del_queue = []
        for k in self.lreg.keys():
            print(k, type(k))
            if time.time() > self.lreg[k].del_time:
                if debug:
                    printf('Deleting local cache value: "%s"\n', self.lreg[k].name)
                # del self.lreg[k]
                del_queue.append(k)

        while len(del_queue) > 0:
            name = del_queue.pop()
            del self.lreg[name]
        for k in self.mreg.keys():
            if time.time() > self.mreg[k].del_time:
                if debug:
                    printf('Deleting my cache value: "%s"\n', self.mreg[k].name)
                del_queue.append(k)
        while len(del_queue) > 0:
            name = del_queue.pop()
            # notify
            pack = Packet(Packet.EXPIRE, 0, 0, name, self.addr, None)
            self.bcastt_sock.send_multipart(
                ['lcache'.encode('utf8'), pack.binarize()])
            # delete
            del self.mreg[name]
        # enqueue the next event in 1 second
        self.scheduler.enter(DContext.EXPIRE_EVENT, 0, self._ttl_sweep, ())

    def _autosub(self):
        if debug:
            printf("Sending subscription messages.\n")

        # printf('Binding to \"udp://%s:%d\"\n', DContext.BCAST_ADDR, DContext.BCAST_PORT)
        # self.bcastr_sock.connect('udp://%s:%d' % (DContext.BCAST_ADDR, DContext.BCAST_PORT))
        f3 = self.addr.rsplit('.', 1)[0]
        for i in range(1, 255):
            # printf("Connecting to \"tcp://%s.%d:%d\"\n", f3, i, DContext.BCAST_PORT)
            self.bcastr_sock.connect('tcp://%s.%d:%d' % (f3, i, DContext.BCAST_PORT))
        self.bcastr_sock.setsockopt(zmq.UNSUBSCRIBE, b'lcache')
        self.bcastr_sock.setsockopt(zmq.SUBSCRIBE, b'lcache')
        # enqueue the next check in 30 seconds
        # self.scheduler.enter(DContext.BCAST_EVENT, 1, self._autosub, ())

    def _dcom_listen(self):
        while self.is_running:
            try:
                msg = self.dcomr_sock.recv(zmq.NOBLOCK)
            except:
                time.sleep(0.005)
                continue
            try:
                pack = Packet.debinarize(msg)
                if(pack.msg_type == Packet.LQUERY):
                    if debug:
                        printf("Query request on \"%s\" from \"%s\"\n", pack.name.decode('utf8'), pack.sender.decode('utf8'))
                    if pack.name.decode('utf8') in self.mreg:  # if I have it in my own cache
                        citem = self.mreg[pack.name.decode('utf8')]
                        rpack = Packet(
                            pack.msg_type,
                            1,
                            citem.del_time,
                            citem.name,
                            self.addr,
                            citem.data)
                    else:
                        rpack = Packet(
                            pack.msg_type,
                            0,
                            0,
                            pack.name,
                            self.addr,
                            None)
                    self.dcomr_sock.send(rpack.binarize())
                else:
                    self.dcomr_sock.send(bytearray(0))
            except SyntaxError as e:
                print('Raw Packet: ', msg)
                print("SyntaxError: ", e)
                self.dcomr_sock.send(bytearray(0))  # provide reply

    def _icom_listen(self):
        while self.is_running:
            try:
                msg = self.icom_sock.recv(zmq.NOBLOCK)
            except:
                time.sleep(0.005)
                continue
            try:
                pack = Packet.debinarize(msg)
                if pack.msg_type == Packet.MQUERY:
                    if debug:
                        printf('Received internal query for \"%s\"\n', pack.name.decode('utf8'))
                    if pack.name.decode('utf8') in self.mreg:
                        citem = self.mreg[pack.name.decode('utf8')]
                        rpack = Packet(
                            pack.msg_type,
                            1,
                            citem.del_time,
                            pack.name,
                            self.addr,
                            citem.data)
                        self.icom_sock.send(rpack.binarize())
                    elif pack.name.decode('utf8') in self.lreg:
                        citem = self.lreg[pack.name.decode('utf8')]

                        dcomt = self.ctx.socket(zmq.REQ)
                        dcomt.setsockopt(zmq.IPV4ONLY, True)

                        dcomt.connect("tcp://%s:%d" % (
                            citem.owner, DContext.DCOM_PORT))

                        qpack = Packet(
                            Packet.LQUERY,
                            0,
                            citem.del_time,
                            citem.name,
                            self.addr,
                            None)

                        if debug:
                            print('Sending query to: ', citem.owner, ' for: ', citem.name)

                        dcomt.send(qpack.binarize())
                        qpack = Packet.debinarize(dcomt.recv())
                        dcomt.close()

                        if qpack.success == 1:

                            if debug:
                                print('Succeeded in fetching data from: ', citem.owner, ' for: ', citem.name)
                            rpack = Packet(
                                pack.msg_type,
                                1,
                                qpack.del_time,
                                qpack.name,
                                self.addr,
                                qpack.data)

                            del self.lreg[pack.name.decode('utf8')]
                            litem = CacheItem(
                                qpack.name.decode('utf8'),
                                qpack.del_time,
                                qpack.data,
                                self.addr)
                            self.mreg[citem.name] = litem
                        else:
                            rpack = pack
                            rpack.success = 0
                        self.icom_sock.send(rpack.binarize())
                    else:
                        rpack = Packet(
                            pack.msg_type,
                            0,
                            0,
                            pack.name.decode('utf8'),
                            self.addr,
                            None)
                        self.icom_sock.send(rpack.binarize())
                elif pack.msg_type == Packet.MCACHE:
                    if debug:
                        printf("Received data to cache internally, \"%s\"\n", pack.name.decode('utf8'))

                    if pack.name.decode('utf8') in self.mreg:
                        del self.mreg[pack.name.decode('utf8')]
                    if pack.name.decode('utf8') in self.lreg:
                        del self.lreg[pack.name.decode('utf8')]

                    citem = CacheItem(
                        pack.name.decode('utf8'),
                        pack.del_time,
                        pack.data,
                        pack.sender.decode('utf8'))
                    self.mreg[citem.name] = citem

                    bpack = Packet(
                        Packet.CACHED,
                        1,
                        citem.del_time,
                        citem.name,
                        self.addr,
                        None)

                    self.bcastt_sock.send_multipart(
                        ['lcache'.encode('utf8'), bpack.binarize()])

                    rpack = Packet(
                        pack.msg_type,
                        1,
                        0,
                        citem.name,
                        self.addr,
                        None)

                    self.icom_sock.send(rpack.binarize())
                else:
                    self.icom_sock.send(None)
            except SyntaxError as e:
                print('Raw Packet: ', msg)
                print("SyntaxError: ", e)
                self.icom_sock.send(None)  # provide reply

    def _cast_listen(self):
        while self.is_running:
            try:
                topic, msg = self.bcastr_sock.recv_multipart(zmq.NOBLOCK)
            except:
                time.sleep(0.005)
                continue
            try:
                pack = Packet.debinarize(msg)
                if pack.sender.decode('utf8') == self.addr:  #self message
                    continue
                if pack.msg_type == Packet.EXPIRE:
                    if debug:
                        printf("Received expiry message on \"%s\"\n", pack.name.decode('utf8'))
                    while pack.name.decode('utf8') in self.lreg:
                        del self.lreg[pack.name.decode('utf8')]
                elif pack.msg_type == Packet.CACHED:
                    if debug:
                        printf("Received cache notification on \"%s\"\n", pack.name.decode('utf8'))
                    citem = CacheItem(
                        pack.name.decode('utf8'),
                        pack.del_time,
                        None,
                        pack.sender.decode('utf8'))
                    if pack.name.decode('utf8') in self.lreg:
                        del self.lreg[pack.name.decode('utf8')]
                    self.lreg[pack.name.decode('utf8')] = citem
            except SyntaxError as e:
                print('Raw Packet: ', msg)
                print("SyntaxError: ", e)

    def __del__(self):
        while len(self.scheduler.queue) > 0:
            self.scheduler.cancel(self.scheduler.queue[0])
        self.ctx.term()


def main():
    try:
        dctx = DContext()
        dctx.start()
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Caught keyboard interrupt.  Shutting down...")
        if dctx is not None:
            dctx.stop()
        exit(0)


def printf(s, *vargs):
    return sys.stdout.write(s % vargs)

if __name__ == '__main__':

    debug = False
    if len(sys.argv) > 1:
        if sys.argv[1] == 'test':
            debug = True

    main()
