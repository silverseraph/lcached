#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "../common/common_internal.h"
#include "context.h"

#ifndef __remote_packed_array_header__
#define __remote_packed_array_header__

typedef struct _remote_packed_array remote_packed_array;

struct _remote_packed_array
{

};

#endif
