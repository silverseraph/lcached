#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <glib.h>

#include "../common/common_internal.h"

#ifndef __priority_queue_header__
#define __priority_queue_header__

#define PRIORITY_QUEUE_INITIAL_SIZE (1024)

typedef struct priority_queue_entry_t priority_queue_entry;
typedef struct priority_queue_t priority_queue;

struct priority_queue_entry_t
{
    float priority;
    void *data;
};

struct priority_queue_t
{
    REF_COUNT;

    int _count;
    int _size;
    priority_queue_entry **_data;

    GFreeFunc _data_free;
};

priority_queue *priority_queue_new(GFreeFunc data_free);

REF_COUNT_HEAD(priority_queue)

void priority_queue_clear(priority_queue *self);

void priority_queue_offer(priority_queue *self, float priority, void *data);

void *priority_queue_poll(priority_queue *self, float *priority);

#endif
