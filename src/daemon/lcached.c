#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "args.h"
#include "context.h"
#include "config.h"

int main(int argc, char *argv[])
{
    //parse args and startup the context
    args *a = args_parse(argc, argv);
    context *c = context_new(a);

    //initialize the config
    config_init(&daemon_config);
    config_add_args(&daemon_config, a);
    if(!config_validate(&daemon_config))
    {
        log_fubar("Errors exist in the configuration and/or args.  Exiting...\n");
    }

    //start daemon

    context_free(c);
    args_unref(a);
    return 0;
}
