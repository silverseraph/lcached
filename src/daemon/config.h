#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "args.h"
#include "../common/paths.h"

#ifndef __config_header__
#define __config_header__

typedef struct _config config;

extern config daemon_config;

struct _config
{
    int super_line_size;         // multiple of 1 kbyte
    int parallel_client_submits; // parallel internal submits, minimum 1
    int parallel_client_queries; // parallel internal queries, minimum 1
    int parallel_daemon_queries; // parallel daemon handling, minimum 1
    int parallel_daemon_message; // parallel broadcast reading, minimum 1

    size_t max_cache_size;       // total space allowed, in kbytes

    char *log_dst;               // destination for log messages

    char *eviction_method;       // The eviction method to use [LRU, LFU, MQ]
    float eviction_method_arg;   // A modification to the eviction method
};

#define DEFAULT_CONFIG_FILE (LCACHE_CONFIG_PATH "/lcached.conf")

#define CONFIG_READ_ERROR(name, err) \
    do { \
        log_err("Config: " name " -> %s\n", (err)->message); \
        g_error_free(err); \
    } while(0)

#define DEFAULT_EVICTION_METHOD "lru"

void config_init(config *self);

void config_read(config *self, char *file);

bool config_validate(config *self);

void config_add_args(config *self, args *a);

#endif
