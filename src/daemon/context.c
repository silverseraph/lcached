#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "context.h"
#include "args.h"

context *context_new(args *a)
{
    context *self = (context*)malloc(sizeof(context));

    self->zmq_ctx = zmq_ctx_new();
    self->args = args_ref(a);
    self->dcom_sock = zmq_socket(self->zmq_ctx, ZMQ_DEALER);
    self->icom_sock = zmq_socket(self->zmq_ctx, ZMQ_DEALER);

    return self;
}

void context_free(context *self)
{
    if(self)
    {
        if(self->dcom_sock) zmq_close(self->dcom_sock);
        if(self->icom_sock) zmq_close(self->icom_sock);
        if(self->args) args_unref(self->args);
        free(self);
    }
}
