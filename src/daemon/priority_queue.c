#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <glib.h>

#include "../common/common_internal.h"

#include "priority_queue.h"

priority_queue *priority_queue_new(GFreeFunc data_free)
{
    priority_queue *self = (priority_queue*)malloc(sizeof(priority_queue));

    REF_COUNT_INIT;

    self->_size = PRIORITY_QUEUE_INITIAL_SIZE;
    self->_count = 0;
    self->_data = (priority_queue_entry**)malloc(sizeof(priority_queue_entry*) * self->_size);
    self->_data_free = data_free;

    return self;
}

static void priority_queue_free(priority_queue *self)
{
    if(self)
    {
        priority_queue_clear(self);

        if(self->_data) free(self->_data);

        free(self);
    }
}

REF_COUNT_IMP(priority_queue)

void priority_queue_clear(priority_queue *self)
{
    int i;
    for(i = 0; i < self->_count; i++)
    {
        self->_data_free(self->_data[i]->data);
        free(self->_data[i]);
        self->_data[i] = NULL;
    }
}

static inline bool priority_queue__idx_valid(priority_queue *self, int idx)
{
    return ((idx > -1) && (idx < self->_count));
}

static inline int priority_queue__parent(priority_queue *self, int child)
{
    unused(self);

    if(child != 0)
        return (child-1) >> 1;
    else
        return -1;
}

static inline int priority_queue__left(priority_queue *self, int parent)
{
    int idx = (parent << 1) + 1;
    if(priority_queue__idx_valid(self, idx))
        return idx;
    else
        return -1;
}

static inline int priority_queue__right(priority_queue *self, int parent)
{
    int idx = (parent << 1) + 2;
    if(priority_queue__idx_valid(self, idx))
        return idx;
    else
        return -1;
}

static priority_queue_entry *priority_queue_entry_new(float priority, void *data)
{
    priority_queue_entry *self = (priority_queue_entry*)malloc(sizeof(priority_queue_entry));

    self->priority = priority;
    self->data = data;

    return self;
}

static void priority_queue__bubbleup(priority_queue *self, int idx)
{
    while((idx > 0) && (priority_queue__parent(self, idx) >= 0) && (self->_data[priority_queue__parent(self, idx)]->priority > self->_data[idx]->priority))
    {
        priority_queue_entry *data = self->_data[priority_queue__parent(self, idx)];
        self->_data[priority_queue__parent(self, idx)] = self->_data[idx];
        self->_data[idx] = data;
        idx = priority_queue__parent(self, idx);
    }
}

static void priority_queue__trickledown(priority_queue *self, int idx)
{
    int left_idx = priority_queue__left(self, idx);
    if(!priority_queue__idx_valid(self, left_idx)) return;

    int smaller_idx = left_idx;
    int right_idx = priority_queue__right(self, idx);

    if(priority_queue__idx_valid(self, right_idx))
    {
        if(self->_data[right_idx]->priority < self->_data[left_idx]->priority)
        {
            smaller_idx = right_idx;
        }
    }

    if(self->_data[smaller_idx]->priority < self->_data[idx]->priority)
    {
        priority_queue_entry *data = self->_data[idx];
        self->_data[idx] = self->_data[smaller_idx];
        self->_data[smaller_idx] = data;
        priority_queue__trickledown(self, smaller_idx);
    }
}

void priority_queue_offer(priority_queue *self, float priority, void *data)
{
    //if at capacity, double capacity
    if(self->_size == self->_count)
    {
        self->_size <<= 1;
        self->_data = (priority_queue_entry**)realloc(self->_data, sizeof(priority_queue_entry*) * self->_size);

        if(self->_data == NULL)
            log_fubar("Failed to realloc priority queue properly.\n");
    }

    self->_data[self->_count] = priority_queue_entry_new(priority, data);

    priority_queue__bubbleup(self, self->_count);

    self->_count++;
}

void *priority_queue_poll(priority_queue *self, float *priority)
{
    float i_priority = 0;
    void *item = NULL;

    if(self->_count == 0) goto end;

    if((self->_count < (self->_size << 2)) && self->_count > PRIORITY_QUEUE_INITIAL_SIZE)
    {
        log_nifty("Cutting down size of priority_queue.\n");
        self->_size >>= 1;
        self->_data = (priority_queue_entry**)realloc(self->_data, sizeof(priority_queue_entry*) * self->_size);
        if(self->_data == NULL)
            log_fubar("Failed to realloc priority queue properly.\n");
    }

    //retrieve data
    priority_queue_entry* data = self->_data[0];
    item = data->data;
    i_priority = data->priority;
    free(data);

    //move tail element to head + heapify
    self->_data[0] = self->_data[self->_count-1];
    self->_count--;
    priority_queue__trickledown(self, 0);

end:
    if(priority) *priority = i_priority;
    return item;
}
