#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "../common/common_internal.h"

#ifndef __args_header__
#define __args_header__

typedef struct _args args;

struct _args
{
    REF_COUNT;

    bool debug;

    bool max_cache_size_set;
    bool super_line_size_set;
    bool parallel_client_submits_set;
    bool parallel_client_queries_set;
    bool parallel_daemon_queries_set;
    bool parallel_daemon_message_set;
    bool eviction_method_set;
    bool eviction_method_arg_set;

    size_t max_cache_size;
    int super_line_size;
    int parallel_client_submits;
    int parallel_client_queries;
    int parallel_daemon_queries;
    int parallel_daemon_message;
    char *eviction_method;
    float eviction_method_arg;

    char *config_file;
};

REF_COUNT_HEAD(args)

args *args_parse(int argc, char *argv[]);

void args_show_help();

#define REQUIRE_OPTION(name) log_warn("%s requires an option.\n", (name))

#endif
