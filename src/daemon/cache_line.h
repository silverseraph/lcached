#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "../common/common_internal.h"
#include "remote_packed_array.h"
#include "context.h"

#ifndef __cache_line_header__
#define __cache_line_header__

typedef struct _cache_line cache_line;

struct _cache_line
{
    char *name;
    size_t len;
    remote_packed_array *rpa;
};

cache_line *cache_line_new(context *c);

#endif
