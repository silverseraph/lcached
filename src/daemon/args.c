#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "args.h"
#include "../common/common_internal.h"

static args *args_new()
{
    args *self = (args*)malloc(sizeof(args));
    REF_COUNT_INIT;

    self->debug = false;
    self->max_cache_size_set = false;
    self->super_line_size_set = false;
    self->parallel_client_submits_set = false;
    self->parallel_client_submits_set = false;
    self->parallel_daemon_queries_set = false;
    self->parallel_daemon_message_set = false;
    self->eviction_method_set = false;
    self->eviction_method_arg_set = false;

    self->max_cache_size = 1024;
    self->super_line_size = 1;
    self->parallel_client_submits = 1;
    self->parallel_client_queries = 1;
    self->parallel_daemon_queries = 1;
    self->parallel_daemon_message = 1;
    self->eviction_method = "lru";
    self->eviction_method_arg = 1.0f;

    self->config_file = NULL;

    return self;
}

static void args_free(args *self)
{
    if(self)
    {
        free(self);
    }
}

REF_COUNT_IMP(args)

args *args_parse(int argc, char *argv[])
{
    int i;
    bool show_help = false;
    char *lowered = NULL, *tmp = NULL;
    args *self = args_new();

    for(i = 1; i < argc; i++)
    {
        if(0 == strcmp(argv[i], "-h") || 0 == strcmp(argv[i], "--help"))
        {
            show_help = true;
        }

        if(0 == strcmp(argv[i], "-m") || 0 == strcmp(argv[i], "--max-cache"))
        {
            i++;
            if(i < argc)
            {
                self->max_cache_size_set = true;
                self->max_cache_size = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-m || --max-cache");
        }

        if(0 == strcmp(argv[i], "-s") || 0 == strcmp(argv[i], "--super-line"))
        {
            i++;
            if(i < argc)
            {
                self->super_line_size_set = true;
                self->super_line_size = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-s || --super-line");
        }

        if(0 == strcmp(argv[i], "-c") || 0 == strcmp(argv[i], "--client-submits"))
        {
            i++;
            if(i < argc)
            {
                self->parallel_client_submits_set = true;
                self->parallel_client_submits = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-c || --client-submits");
        }

        if(0 == strcmp(argv[i], "-q") || 0 == strcmp(argv[i], "--client-queries"))
        {
            i++;
            if(i < argc)
            {
                self->parallel_client_queries_set = true;
                self->parallel_client_queries = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-q || --client-queries");
        }

        if(0 == strcmp(argv[i], "-r") || 0 == strcmp(argv[i], "--daemon-queries"))
        {
            i++;
            if(i < argc)
            {
                self->parallel_daemon_queries_set = true;
                self->parallel_daemon_queries = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-r || --daemon-queries");
        }

        if(0 == strcmp(argv[i], "-b") || 0 == strcmp(argv[i], "--daemon-message"))
        {
            i++;
            if(i < argc)
            {
                self->parallel_daemon_queries_set = true;
                self->parallel_daemon_queries = atoi(argv[i]);
            }
            else REQUIRE_OPTION("-b || --daemon-message");
        }

        if(0 == strcmp(argv[i], "-d") || 0 == strcmp(argv[i], "--debug"))
        {
            self->debug = true;
        }

        if(0 == strcmp(argv[i], "-f") || 0 == strcmp(argv[i], "--config-file"))
        {
            i++;
            if(i < argc)
            {
                self->config_file = argv[i];
            }
            else REQUIRE_OPTION("-f || --config-file");
        }

        if(0 == strcmp(argv[i], "-e") || 0 == strcmp(argv[i], "--eviction"))
        {
            i++;
            if(i < argc)
            {
                lowered = g_utf8_strdown(argv[i], -1);

                if(0 == strcmp(lowered, "lru") || 
                   0 == strcmp(lowered, "lfu") ||
                   0 == strcmp(lowered, "mq"))
                {
                    self->eviction_method_set = true;
                    self->eviction_method = lowered;
                }
                else
                {
                    log_err("The -e || --eviction argument requires one of [lru, lfu, mq] as an option.\n");
                }
            }
            else REQUIRE_OPTION("-e || --eviction");
        }

        if(0 == strcmp(argv[i], "-a") || 0 == strcmp(argv[i], "--evict-arg"))
        {
            i++;
            if(i < argc)
            {
                self->eviction_method_arg_set = true;
                self->eviction_method_arg = atof(argv[i]);
            }
            else REQUIRE_OPTION("-a || --evict-args");
        }
    }

    if(show_help)
    {
        args_show_help();
        exit(0);
    }

    return self;
}

void args_show_help()
{
    fprintf(stdout,
"   Usage: lcached [args...]\n"
"       -h || --help                 - Display the help after parsing args, then exit.\n"
"       -m || --max-cache      [int] - The maximum cache size, as a multiple of 1KByte\n"
"       -s || --super-line     [int] - The multiplier for the cache line size\n"
"       -c || --client-submits [int] - Maximum parallel data pushes to daemon from local client\n"
"       -q || --client-queries [int] - Maximum parallel queries from the local client to local daemon\n"
"       -r || --daemon-queries [int] - Maximum parallel queries from other daemons\n"
"       -b || --daemon-message [int] - Maximum parallel broadcasts to interpret\n"
"       -d || --debug                - Enable debug mode.\n"
"       -f || --config-file   [path] - The path of the config file to load.  Replaces the default config. \n"
"       -e || --eviction    [method] - Select the cache eviction method [LRU, LFU, MQ] \n"
"       -a || --evict-arg    [float] - An argument value to the eviction method.\n"
);

}
