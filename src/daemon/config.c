#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include <glib.h>

#include "config.h"

config daemon_config;

void config_init(config *self)
{
    memset(self, 0, sizeof(config));
    self->super_line_size = 1;
    self->parallel_client_submits = 1;
    self->parallel_client_queries = 1;
    self->parallel_daemon_queries = 1;
    self->parallel_daemon_message = 1;
    self->eviction_method = g_strdup(DEFAULT_EVICTION_METHOD);
    self->eviction_method_arg = 1;
    self->max_cache_size = self->super_line_size * 1024;
}

void config_read(config *self, char *file)
{
    GError *err = NULL;
    GKeyFile *kf = g_key_file_new();
    int tmp;
    char *tmps;
    double tmpd;

    if(!file) file = DEFAULT_CONFIG_FILE;

    if(!g_file_test(file, G_FILE_TEST_EXISTS))
    {
        log_err("Config: The config file \"%s\" doesn't exist.\n", file);
        return;
    }

    if(!g_key_file_load_from_file(kf, file, G_KEY_FILE_NONE, &err))
    {
        if(err) goto err;

        log_err("Config: no error occurred reading lcached config file, but the key file could not be loaded.");
        return;
    }

    tmp = g_key_file_get_integer(kf, "cache", "super_line_size", &err);
    if(err) CONFIG_READ_ERROR("super_line_size", err);
    else    self->super_line_size = tmp;

    tmp = g_key_file_get_integer(kf, "connections", "parallel_client_queries", &err);
    if(err) CONFIG_READ_ERROR("parallel_client_queries", err);
    else    self->parallel_client_queries = tmp;

    tmp = g_key_file_get_integer(kf, "connections", "parallel_client_submits", &err);
    if(err) CONFIG_READ_ERROR("parallel_client_submits", err);
    else    self->parallel_client_submits = tmp;

    tmp = g_key_file_get_integer(kf, "connections", "parallel_daemon_queries", &err);
    if(err) CONFIG_READ_ERROR("parallel_daemon_queries", err);
    else    self->parallel_daemon_queries = tmp;

    tmp = g_key_file_get_integer(kf, "connections", "parallel_daemon_message", &err);
    if(err) CONFIG_READ_ERROR("parallel_daemon_message", err);
    else    self->parallel_daemon_message = tmp;

    tmp = g_key_file_get_integer(kf, "cache", "max_cache_size", &err);
    if(err) CONFIG_READ_ERROR("max_cache_size", err);
    else    self->max_cache_size = tmp;

    tmps = g_key_file_get_string(kf, "cache", "eviction_method", &err);
    if(err) CONFIG_READ_ERROR("eviction_method", err);
    else    self->eviction_method = tmps;

    tmpd = g_key_file_get_double(kf, "cache", "eviction_method_arg", &err);
    if(err) CONFIG_READ_ERROR("eviction_method_arg", err);
    else    self->eviction_method_arg = (float)tmpd;

    return;

    err:
    log_err("Config: problem reading key file: %s\n", err->message);
    g_error_free(err);
}

bool config_validate(config *self)
{
    if(self->super_line_size < 1)
    {
        self->super_line_size = 1;
        log_err("Config: super line size must be a positive number\n");
    }
    if(self->parallel_client_submits < 1)
    {
        self->parallel_client_submits = 1;
        log_err("Config: parallel client submission connections must be greater than zero.\n");
    }
    if(self->parallel_client_queries < 1)
    {
        self->parallel_client_queries = 1;
        log_err("Config: parallel client query connections must be greater than zero.\n");
    }
    if(self->parallel_daemon_queries < 1)
    {
        self->parallel_daemon_queries = 1;
        log_err("Config: parallel incoming daemon queries must be greater than zero.\n");
    }

    if(self->parallel_daemon_message < 1)
    {
        self->parallel_daemon_message = 1;
        log_err("Config: parallel incoming daemon broadcasts must be greater than zero.\n");
    }

    //don't allow non-natural numbers for space
    if((int)self->max_cache_size < self->super_line_size)
    {
        log_err("Config: maximum cache size must be a positive number at least equal to the super line size.\n");
        self->max_cache_size = self->super_line_size;
    }

    //round up uneven divisors
    if((self->max_cache_size % self->super_line_size) != 0)
    {
        log_err("Config: maximum cache size must be a multiple of the super line size.\n");
        self->max_cache_size = ((self->max_cache_size / self->super_line_size) + 1) * self->super_line_size;
    }

    if(0 != strcmp(self->eviction_method, "lru") &&
       0 != strcmp(self->eviction_method, "lfu") &&
       0 != strcmp(self->eviction_method, "mq"))
    {
        log_err("Config: eviction method must be one of [lru, lfu, mq].\n");
        self->eviction_method = g_strdup(DEFAULT_EVICTION_METHOD);
    }

    return true;
}

void config_add_args(config *self, args *a)
{
    config_read(self, a->config_file);

    if(a->max_cache_size_set)
        self->max_cache_size = a->max_cache_size;
    if(a->super_line_size_set)
        self->super_line_size = a->super_line_size;
    if(a->parallel_client_submits_set)
        self->parallel_client_submits = a->parallel_client_submits;
    if(a->parallel_client_queries_set)
        self->parallel_client_queries = a->parallel_client_queries;
    if(a->parallel_daemon_queries_set)
        self->parallel_daemon_queries = a->parallel_daemon_queries;
    if(a->parallel_daemon_message_set)
        self->parallel_daemon_message = a->parallel_daemon_message;
    if(a->eviction_method_set)
        self->eviction_method = g_strdup(a->eviction_method);
    if(a->eviction_method_arg_set)
        self->eviction_method_arg = a->eviction_method_arg;
}
