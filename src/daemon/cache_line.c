#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "../common/common_internal.h"
#include "cache_line.h"
#include "context.h"

cache_line *cache_line_new(context *c)
{
    cache_line *self = (cache_line*)malloc(sizeof(cache_line));
    memset(self, 0, sizeof(cache_line));
    return self;
}
