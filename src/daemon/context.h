#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "args.h"

#ifndef __context_header__
#define __context_header__

#define BCAST_PORT (1333)
#define DCOM_PORT (1334)
#define ICOM_PORT (1335)

typedef struct {
    void *zmq_ctx;
    void *dcom_sock;
    void *icom_sock;

    args *args;
} context;

context *context_new();

void context_free(context *self);

#endif
