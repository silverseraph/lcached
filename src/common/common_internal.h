#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <glib.h>
#include "ref_count.h"
#include "color.h"
#include "logging.h"

#ifndef __common_internal_header__
#define __common_internal_header__

#define STRN(a) #a

typedef void (*free_func)(void*);

static bool big_endian()
{
    int n = 1;
    int *np = &n;
    char *r = (char*)np;
    return (*r != 1);
}

#define unused(var) (void)var

#endif
