#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <glib.h>

#ifndef __refcount_header__
#define __refcount_header__

/* BEGIN DEFINITON OF REF COUNT UTILITIES */
//for use only in header files
#define REF_COUNT_HEAD(type) \
    void type##_unref(type *self); \
    type *type##_ref(type *self);

//for use only in .c files as the implementation
#define REF_COUNT_IMP(type) \
    void type##_unref(type *self) \
    { \
        if(g_atomic_int_dec_and_test(&self->ref_count)) \
        { \
            type##_free(self); \
        } \
    } \
    type *type##_ref(type *self) \
    { \
        g_atomic_int_add(&self->ref_count, 1); \
        return self; \
    }

//use in defining the structure
#define REF_COUNT \
    int ref_count

//use in init/new function
#define REF_COUNT_INIT \
    self->ref_count = 1
/* END DEFINITON OF REF COUNT UTILITIES */


#endif
