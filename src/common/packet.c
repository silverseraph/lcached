#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

#include <zmq.h>

#include "../common/common_internal.h"

#include "packet.h"

packet *packet_new()
{
    packet *self =  (packet*)malloc(sizeof(packet));
    memset(self, 0, sizeof(packet));
    return self;
}

packet *packet_from_zmq_msg(zmq_msg_t *msg)
{
    size_t size = 0;
    packet *pack = zmq_msg_data(msg);

    size = pack->name_len + pack->sender_len + pack->data_len;

    if(size == zmq_msg_size(msg))
        return pack;
    else
        return NULL;
}

static void zmq_pack_free(void *data, void *hint)
{
    (void)hint;
    free(data);
}

zmq_msg_t *packet_get_zmq_msg(packet *pack)
{
    size_t raw_len = 8 + (pack->name_len + pack->sender_len + pack->data_len);
    uint8_t *raw = (uint8_t*)malloc(raw_len);
    zmq_msg_t *msg = (zmq_msg_t*)malloc(sizeof(zmq_msg_t));
    int i, off;
    
    raw[0] = pack->msg_type;
    raw[1] = pack->success;
    raw[4] = pack->name_len;
    raw[5] = pack->sender_len;

    if(big_endian())
    {
        raw[2] = pack->del_time >> 8;
        raw[3] = pack->del_time & 0xFF;
        raw[6] = pack->data_len >> 8;
        raw[7] = pack->data_len & 0xFF;

        off = 7 + pack->name_len;
        for(i = 0; i < pack->name_len; i++)
            raw[off - i] = pack->name[i];

        off += pack->sender_len;
        for(i = 0; i < pack->sender_len; i++)
            raw[off - i] = pack->sender[i];

        off += pack->data_len;
        for(i = 0; i < pack->data_len; i++)
            raw[off - i] = pack->data[i];
    }
    else
    {
        raw[2] = pack->del_time & 0xFF;
        raw[3] = pack->del_time >> 8;
        raw[6] = pack->data_len & 0xFF;
        raw[7] = pack->data_len >> 8;
        memcpy(&raw[8], pack->name, pack->name_len);
        memcpy(&raw[8+pack->name_len], pack->sender, pack->sender_len);
        memcpy(&raw[8+pack->name_len+pack->sender_len], pack->data, pack->data_len);
    }

    zmq_msg_init_data(msg, raw, raw_len, zmq_pack_free, NULL);

    return msg;
}
