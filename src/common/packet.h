#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

#include <zmq.h>

#include "../common/common_internal.h"

#ifndef __packet_header__
#define __packet_header__

typedef struct
{
    uint8_t msg_type;
    uint8_t success;
    uint16_t del_time;
    uint8_t name_len;
    uint8_t sender_len;
    uint16_t data_len;

    char *name;
    char *sender;
    uint8_t *data;
} packet;

packet *packet_new();

void packet_free(packet *self);

packet *packet_from_zmq_msg(zmq_msg_t *msg);

zmq_msg_t *packet_get_zmq_msg(packet *pack);

#endif
