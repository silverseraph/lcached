#ifndef __port_header__
#define __port_header__

//internal communication port
#define IPORT (1333)
#define STR_IPORT STRN(IPORT)

//daemon-to-daemon communication port
#define DPORT (1334)
#define STR_DPORT STRN(DPORT)

//broadcast port
#define BPORT (1335)
#define STR_BPORT STRN(BPORT)

#endif
