#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include <glib.h>

#include "color.h"

#ifndef __logging_header__
#define __logging_header__

//definitions of errors as equilength so that logging isn't so fucked
#define ERROR "[ERROR]: "
#define WARN  "[WARN]:  "
#define INFO  "[INFO]:  "
#define NIFTY "[NIFTY]: "
#define FUBAR "[FUBAR]: "

#define log_fubar(M, ...) \
    do { \
        fprintf(stderr, MAGENTA FUBAR "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
        exit(EXIT_FAILURE); \
    } while(0)

//always show errors.  your face is about to melt off
#define log_err(M, ...)   \
    do { \
        fprintf(stderr, ORANGE  ERROR "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
    } while(0)

//always show warnings.  these are bad, but not terrible
#define log_warn(M, ...)  \
    do { \
        fprintf(stderr, BLUE    WARN  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
    } while(0)

//show interesting events always, use sparingly
#define log_nifty(M, ...) \
    do { \
        fprintf(stdout, PURPLE  NIFTY "(%s) "       M RESET,                     __FUNCTION__, ##__VA_ARGS__); \
    } while(0)

//show debug events, go crazy with this
#ifdef DEBUG
#define log_info(M, ...)  \
    do { \
        fprintf(stdout, GREEN   INFO  "(%s:%d:%s) " M RESET, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
    } while(0)
#else
#define log_info(M, ...)
#endif

#endif
