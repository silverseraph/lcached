#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "../common/common_internal.h"

#ifndef __queue_header__
#define __queue_header__

typedef struct _node node;

struct _node {
    node *next;
    void *data;
    node *prev;
};

typedef struct {
    node *head;
    node *tail;
    int length;
} queue;

queue *queue_new();

void queue_free(queue *self);

void queue_full_free(queue *self, free_func func);

void queue_push_head(queue *self, void *data);

void queue_push_tail(queue *self, void *data);

void *queue_pop_head(queue *self);

void *queue_pop_tail(queue *self);

#endif
