#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "queue.h"

queue *queue_new()
{
    queue *self = (queue*)malloc(sizeof(queue));
    memset(self, 0, sizeof(queue));

    self->head = NULL;
    self->tail = NULL;
    self->length = 0;

    return self;
}

void queue_free(queue *self)
{
    node *curr = NULL, *next = NULL;
    if(self)
    {
        curr = self->head;
        while(curr)
        {
            next = curr->next;
            free(curr);
            curr = next;
        }

        free(self);
    }
}

void queue_full_free(queue *self, free_func func)
{
    node *curr = NULL, *next = NULL;
    if(self)
    {
        curr = self->head;
        while(curr)
        {
            next = curr->next;
            func(curr->data);
            free(curr);
            curr = next;
        }

        free(self);
    }
}

void queue_push_head(queue *self, void *data)
{
    node *new_head = (node*)malloc(sizeof(node));
    new_head->prev = NULL;
    new_head->data = data;
    new_head->next = self->head;
    self->head = new_head;
    self->length++;
}

void queue_push_tail(queue *self, void *data)
{
    node *new_tail = (node*)malloc(sizeof(node));
    new_tail->prev = self->tail;
    new_tail->data = data;
    new_tail->next = NULL;
    self->tail = new_tail;
    self->length++;
}

void *queue_pop_head(queue *self)
{
    node *ret_node = NULL;

    if(self->length == 1)
    {
        ret_node = self->head;
        self->head = NULL;
        self->tail = NULL;
        self->length--;
    }
    else if(self->length > 1)
    {
        ret_node = self->head;
        self->head = self->head->next;
        self->length--;
    }

    if(!ret_node)
        return NULL;
    else
        return ret_node->data;
}

void *queue_pop_tail(queue *self)
{
    node *ret_node = NULL;

    if(self->length == 1)
    {
        ret_node = self->tail;
        self->head = NULL;
        self->tail = NULL;
        self->length--;
    }
    else if(self->length > 1)
    {
        ret_node = self->tail;
        self->tail = self->tail->prev;
        self->length--;
    }

    if(!ret_node)
        return NULL;
    else
        return ret_node->data;
}
