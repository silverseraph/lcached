#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "../common/common_internal.h"
#include "queue.h"

#ifndef __lcached_context__
#define __lcached_context__

typedef struct {
    REF_COUNT;

    void *zmq_ctx;       // zmq context

    int sconnections;    // maximum connected send sockets
    pthread_mutex_t sqm; // mutex for ssocks
    queue *ssocks;       // available sockets for sending
    void **sconnected;   // sockets that are connected for send, whether they are available or not
    sem_t ssem;

    int rconnections;    // maximum connected request sockets
    pthread_mutex_t rqm; // mutex for rsocks
    queue *rsocks;       // available sockets for requesting
    void **rconnected;   // sockets that are connectef for receive, whether they are available or not
    sem_t rsem;

    bool valid;
} lcache_context;

lcache_context *lcached_context_new();

REF_COUNT_HEAD(lcache_context)

bool lcache_context_put(lcache_context *self, void *msg, int msg_len);

bool lcache_context_get(lcache_context *self, void **msg, int *msg_len);

#endif
