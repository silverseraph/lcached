#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include <zmq.h>
#include <zmq_utils.h>

#include "lcache_context.h"
#include "../common/common_internal.h"
#include "../common/ports.h"
#include "../common/packet.h"
#include "queue.h"

static void *make_socket(void *ctx, int recv_timeout, int send_timeout);

lcache_context *lcache_context_new(int scon, int rcon, int recv_timeout, int send_timeout)
{
    int i;
    lcache_context *self = (lcache_context*)malloc(sizeof(lcache_context));
    memset(self, '0', sizeof(lcache_context));

    self->valid = false;

    REF_COUNT_INIT;

    if(scon < 1) scon = 1;
    if(rcon < 1) rcon = 1;

    self->sconnections = scon;
    self->rconnections = rcon;
    self->zmq_ctx = zmq_ctx_new();

    self->ssocks = queue_new();
    pthread_mutex_init(&self->sqm, NULL);
    self->sconnected = (void**)malloc(sizeof(void*) * self->sconnections);
    for(i = 0; i < self->sconnections; i++)
        self->sconnected[i] = make_socket(self->zmq_ctx, recv_timeout, send_timeout);
    sem_init(&self->ssem, 0, self->sconnections);

    self->rsocks = queue_new();
    pthread_mutex_init(&self->rqm, NULL);
    self->rconnected = (void**)malloc(sizeof(void*) * self->rconnections);
    for(i = 0; i < self->rconnections; i++)
        self->rconnected[i] = make_socket(self->zmq_ctx, recv_timeout, send_timeout);
    sem_init(&self->rsem, 0, self->rconnections);

    self->valid = true;
    
    return self;
}

static void lcache_context_free(lcache_context *self)
{
    int sem_value, i;
    if(self)
    {
        //dump out waiting sends quickly
        if(sem_getvalue(&self->ssem, &sem_value) == 0)
        {
            while(sem_value < 0)
                sem_post(&self->ssem);

            sem_destroy(&self->ssem);
        }

        //dump out waiting requests quickly
        if(sem_getvalue(&self->rsem, &sem_value) == 0)
        {
            while(sem_value < 0)
                sem_post(&self->rsem);

            sem_destroy(&self->rsem);
        }

        for(i = 0; i < self->sconnections; i++)
            zmq_close(self->sconnected[i]);

        for(i = 0; i < self->rconnections; i++)
            zmq_close(self->rconnected[i]);

        if(self->zmq_ctx)
        {
            zmq_ctx_destroy(self->zmq_ctx);
        }

        pthread_mutex_destroy(&self->sqm);
        pthread_mutex_destroy(&self->rqm);

        free(self);
    }
}

REF_COUNT_IMP(lcache_context)

static void *make_socket(void *ctx, int recv_timeout, int send_timeout)
{
    int rc;
    void *sock = NULL;
    int *sto = (int*)malloc(sizeof(int));
    int *rto = (int*)malloc(sizeof(int));

    sock = zmq_socket(ctx, ZMQ_REQ);

    *sto = send_timeout;
    *rto = recv_timeout;

    rc = zmq_setsockopt(sock, ZMQ_SNDTIMEO, sto, sizeof(int));
    if(rc) return NULL;
    rc = zmq_setsockopt(sock, ZMQ_RCVTIMEO, rto, sizeof(int));
    if(rc) return NULL;

    rc = zmq_connect(sock, "tcp://127.0.0.1:" STR_IPORT);
    if(rc) return NULL;

    return sock;
}

bool lcache_context_put(lcache_context *self, void *msg, int msg_len)
{
    void *sock;
    zmq_msg_t *out_msg, in_msg;

    //wait for lock
    sem_wait(&self->ssem);

    if(!self->valid)
    {
        sem_post(&self->ssem);
        return false;
    }

    //get socket from waiting queue
    pthread_mutex_lock(&self->sqm);
    sock = queue_pop_head(self->ssocks);
    pthread_mutex_unlock(&self->sqm);

    //perform the actual 'put' request here
    packet *pack = packet_new();

    //return the socket back to the wait queue
    pthread_mutex_lock(&self->sqm);
    queue_push_tail(self->ssocks, sock);
    pthread_mutex_unlock(&self->sqm);

    //pass lock to someone else
    sem_post(&self->ssem);

    return false;
}

bool lcache_context_get(lcache_context *self, void **msg, int *msg_len)
{
    void *sock;

    //wait for lock
    sem_wait(&self->rsem);

    if(!self->valid)
    {
        sem_post(&self->rsem);
        return false;
    }

    //get socket from waiting queue
    pthread_mutex_lock(&self->rqm);
    sock = queue_pop_head(self->rsocks);
    pthread_mutex_unlock(&self->rqm);

    //perform the query here

    
    //add the socket back to the wait queue
    pthread_mutex_lock(&self->rqm);
    queue_push_tail(self->rsocks, sock);
    pthread_mutex_unlock(&self->rqm);

    //pass lock to someone else
    sem_post(&self->rsem);

    return false;
}
