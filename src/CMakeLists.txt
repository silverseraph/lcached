add_definitions(${DEPS_CFLAGS})
link_libraries(${DEPS_LIBRARIES})
link_directories(${DEPS_LIBRARY_DIRS})

include_directories(AFTER .)
include_directories(common)

add_subdirectory(common)
add_subdirectory(api)
add_subdirectory(daemon)
