cmake_minimum_required(VERSION 2.8)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

if(CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS -std=c99)
endif()

set(LCACHE_VERSION_MAJOR "0")
set(LCACHE_VERSION_MINOR "0")
set(LCACHE_VERSION_MICRO "0")

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING
        "Choose the type of build, options are: Debug Release." FORCE)
endif()

set(LCACHE_BUILD_ROOT "${CMAKE_CURRENT_BINARY_DIR}")
set(LCACHE_SRC_ROOT "${CMAKE_CURRENT_SOURCE_DIR}")

set(LCACHE_DEFAULT_VERSION_STRING
    "${LCACHE_VERSION_MAJOR}.${LCACHE_VERSION_MINOR}.${LCACHE_VERSION_MICRO}")

if(UNIX)
    set(MATH_LIB -lm)
else()
    set(MATH_LIB)
endif()

find_package(PkgConfig)
find_package(Threads)

set(modules ${modules} libzmq)
set(modules ${modules} glib-2.0)

pkg_check_modules(DEPS REQUIRED ${modules})

option(BUILD_SHARED "Build the lcache api as a shared library." ON)
set(LCACHE_CONFIG_PATH "/etc/lcache" CACHE PATH "The location for all config files")
set(LCACHE_SHARE_PATH "${CMAKE_INSTALL_DIR}/share" CACHE PATH "The location for shared files")

option(USE_COLOR "Enable colored output" ON)

if(USE_COLOR)
    add_definitions(-DUSE_COLOR)
endif()

add_subdirectory(src)
