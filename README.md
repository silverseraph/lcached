## lcache - A localized, distributed, hot-swappable cache

The purpose of lcache is to provided a memcached-esque interface for clients on a local network to communicate with one another and share information that could potentially be repeated.  I have also implemented the basis of this project, which is located under the `basis` directory.  Further work will all be done using C, so that the daemon and application are light.

The general idea of a host running the lcache daemon is to unify public caches between themselves as well as within themselves.  This would only be meant or public information, however it could help to cache information like images, videos, and static web content.  

The following licenses apply, and may be found under the `license` directory:

* The daemon and all associated code is released under GPLv3.
* All other code, unless otherwise stated, is released under the BSD 2-clause license.
